var CategoryFunctionality = {

  title_string: "Pleco | ",

  set_title: function(category_name) {
    document.title = this.title_string + category_name;
  }

};

var Page = {

  is_category_page: function() {

    if (Category.name()) {
      return true;
    } else {
      return false;
    }

  }

};

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

var Category = {

  name: function() {
    return (("" + window.location).split("?")[1]);
  },

  display_name: function(raw_name) {
    return raw_name.replace("_", " ").capitalize();
  },

  formatted_name: function() {
    this.display_name(this.name())
  },

  show: function(category) {
    $("div[data-categories~=" + category + "]").show();    
    $("div.post").not("[data-categories~=" + category + "]").hide();    
    $(".post-category-title").html(this.display_name(category));
  }

};

$(function() {

  current_category = Category.name();

  if (Page.is_category_page()) {
    category = Category.formatted_name((current_category));
    Category.show(current_category);
  }

  $(".category-link").click(function () {
    Category.show($(this).data("category"));
    CategoryFunctionality.set_title($(this).data("category-name"))
  });

  $(".expandable-link").click(function() {

    var content_segment = $("#" + $(this).data("target")).html();
    $("#content").html(content_segment);

  });

  render_product = function(product) {
               
             return "<div class='col category-2'>\r\n<em class='price'>$" + product["price"] + "</em>"
     	     + "<h2><a href='#'>" + product["name"] + "</a></h2>\r\n" + product["blurb"] + "\r\n<a href='" + product["page"] + "' class='more'>More </a>"
             + "</div>";
  }  

  update_content = function(product_array, mode) {

    switch(mode) {
      case "price":
        p_array = _.sortBy(product_array, function(product) { return product["price"]; });
       break;
      case "product":
        p_array = _.sortBy(product_array, function(product) { return product["name"]; });
       break;
    }  

    x = 0;
    html = "<div class='holder'>\r\n<div class='bg'>\r\n";

    _.each(p_array, function(element) {
 
      if ((x > 0) && (x % 2) == 0) {

        html = html + "</div></div>" + "<div class='holder'><div class='bg'>";

      }

      html = html + render_product(element)

      x = x + 1;

    });

    html = html + "</div></div>";

    $(".catalogue").html( html );

  }

});
