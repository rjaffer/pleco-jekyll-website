module Jekyll
  module DisplayCategoriesFilter
    def display_categories(category_list)      

      return category_list.map { |category_item|
        human_friendly_name = category_item.gsub("_", " ").capitalize
        "<a href = '" + @context.registers[:site].config['url'] + "/category.html?#{category_item}'>#{human_friendly_name}</a>"
      }.join(", ")

    end
  end
end

Liquid::Template.register_filter(Jekyll::DisplayCategoriesFilter)
