require 'rubygems'
require 'yaml'
require 'json'

PRODUCT_PATH = File.expand_path('../../_products', __FILE__)

module Jekyll

  class RenderProductsTag < Liquid::Tag

    def initialize(tag_name, text, tokens)

      super
      @text = text.gsub(/ $/, "")
      @tokens = tokens

      @attributes = {}

        text.scan(Liquid::TagAttributes) do |key, value|
          @attributes[key] = value
        end

    end

    def format(products, format)

      return products.map { |product|

#         return case format
#           when "html"
#             "<div class='col category-2'>\r\n<em class='price'>$" + product["price"].to_s + "</em>
#     	     <h2><a href='#'>" + product["name"] + "</a></h2>\r\n" + product["blurb"] + "\r\n<a href='" + product["page"] + "' class='more'>More </a>
#             </div>"

#           when "json"

             "products.push(" + product.to_json + ");\r\n"

 #        end

      }.join("\r\n")


    end

    def render(context)

      products = Dir.glob("_products/*.yaml").map { |product|

        YAML.load(File.read("#{product}"))
        
      }.keep_if { |product|

        product["category"] == @attributes["category"]

      }

      return format(products, @attributes["format"])

    end

  end

  class RenderProductTag < Liquid::Tag

    def initialize(tag_name, text, tokens)
      super
      @text = text.gsub(/ $/, "")
    end

    def render(context)

      yaml = YAML.load(File.read("_products/#{@text}.yaml"))

      return yaml.to_json

    end
  end
end

Liquid::Template.register_tag('render_product', Jekyll::RenderProductTag)
Liquid::Template.register_tag('render_products', Jekyll::RenderProductsTag)
