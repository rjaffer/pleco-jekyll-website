module Jekyll
  module HumanizeCategoryNameFilter
    def humanize(category_name)      

        return category_name.gsub("_", " ").capitalize

    end
  end
end

Liquid::Template.register_filter(Jekyll::HumanizeCategoryNameFilter)
